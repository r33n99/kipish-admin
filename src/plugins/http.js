import axios from 'axios';

const http = axios.create({
  baseURL:
    process.env.NODE_ENV === 'production'
      ? '/api/v1'
      : 'http://localhost:8081/api/v1',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 180000
});


export default http;
