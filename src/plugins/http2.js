import axios from 'axios';

const http2 = axios.create({
  baseURL:
    process.env.NODE_ENV === 'production'
      ? '/api/v2'
      : 'http://localhost:8081/api/v2',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 180000
});


export default http2;
