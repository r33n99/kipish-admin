import axios from 'axios';

const publicHttp = axios.create({
  baseURL:
    process.env.NODE_ENV === 'production'
      ? '/api/v1'
      : 'http://localhost:8083/api/v1',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 180000
});


export default publicHttp;
