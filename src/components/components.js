import Vue from "vue";
import CustomForm from "@/components/CustomForm.vue";
import DialogForm from "@/components/DialogForm.vue";
import ErrorDialog from "@/components/ErrorDialog.vue";

Object.entries({
    CustomForm,
    DialogForm,
    ErrorDialog
}).forEach(entry => Vue.component(entry[0], entry[1]))