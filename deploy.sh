#!/bin/bash

DEST_DIR=/var/www/adminfront

echo Deploy started...

HOST=root@188.225.80.100
npm run build &&
cd dist && tar -czvf /tmp/frontend.tar.gz ./* && cd ../ &&
  scp /tmp/frontend.tar.gz "$HOST:/tmp/" &&
  ssh "$HOST" "
rm -rf $DEST_DIR &&
mkdir -p $DEST_DIR &&
tar -xzvf /tmp/frontend.tar.gz -C $DEST_DIR
" &&
  rm /tmp/frontend.tar.gz &&
  exit
